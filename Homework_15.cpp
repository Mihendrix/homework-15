﻿ 
#include <iostream>

const int number = 20; 
const bool IsEven = true;
const bool IsOdd = false;

void PrintEvenOrOddNumbers(bool IsEvenOrOdd)
{
	switch (IsEvenOrOdd)
	{
	case IsEven:
		for (int i = 0; i <= number; i += 2)
		{
			std::cout << i << "\n";
		}
		break;

	case IsOdd:
		for (int i = 1; i <= number; i += 2)
		{
			std::cout << i << "\n";
		}
		break;
	}
}

int main()
{
	for (int i = 0; i <= number; i+=2)
	{
		std::cout << i << "\n"; 
	}

	std::cout << "\n";

	PrintEvenOrOddNumbers(IsOdd);
} 


